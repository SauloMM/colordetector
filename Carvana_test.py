import numpy as np
import os 
import gc
from PIL import Image
from glob import glob
import pandas as pd
import matplotlib.pyplot as plt
from skimage.transform import resize
from time import time

from clusters import Cluster
from back import Back
from selector import Selector
from color_detect import ColorDetector


input_path = '../Kaggle_competitions/Carvana Image Masking/'
train_path = os.path.join(input_path, "color_sep_train")
train_masks = os.path.join(input_path, "train_masks")

predictions = []
i = 0
for folder in os.listdir(train_path):
    for item in os.listdir(train_path + '/' + folder):
        start = time()
        img = resize(plt.imread(train_path + '/' + folder + '/' + item), (300,300))
        print("Image " + str(i) + " loaded. Shape: " + str(img.shape))
        
        mask = resize(plt.imread(train_masks + '/' + item[:-4] + '_mask.gif'), (300,300))
        print("Mask " + str(i) + " loaded. Shape: " + str(mask.shape))
        
        im = img[~(mask[:,:,0]  == 0)] #the binary mask itself is in the first channel. By comparing to zero we make it boolean
        print("Number of car pixels: " + str(len(im*255)))
        
        cl = Cluster()
        result = cl.clusterize(im*255)
        print("Centroids found: ")
        print(result)
        print("\n")


        sl = Selector()
        cent = np.array(sl.largest(result[0], result[1], result[2])[0])
        print("Chosen centroids: ")
        print(cent)
        print("\n")


        det = ColorDetector()
        c = det.color_of(cent)

        print("Predicted color: " + c)
        print("True color: " + folder)
        print("\n\n")
        
        print("time: " + str(time() - start))
        predictions.append([c, folder])

with open('preds_file.txt', 'w') as f:
    for item in predictions:
        f.write("%s\n" % item)
print("test done")
